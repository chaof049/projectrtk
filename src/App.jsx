import { Outlet, Route, Routes } from "react-router-dom";
import "./App.css";
import NavBar from "./ProjectComponent/NavBar";
import Footer from "./ProjectComponent/Footer";
import CreateProduct from "./ProjectComponent/CreateProduct";
import ShowAllProducts from "./ProjectComponent/ShowAllProducts";
import ViewProducts from "./ProjectComponent/ViewProducts";
import UpdateProduct from "./ProjectComponent/UpdateProduct";
import { useSelector, useDispatch } from "react-redux";
import { changeName } from "./features/infoSlice";
import { changeCity } from "./features/addressSlice";
import { changeProductName } from "./features/productSlice";
import ShowAllProductsUsingRTK from "./ProjectComponent/ShowAllProductsUsingRTK";
import ViewProductUsingRTK from "./ProjectComponent/ViewProductUsingRTK";
import CreateProductUsingRTK from "./ProjectComponent/CreateProductUsingRTK";
import UpdateProductUsingRTK from "./ProjectComponent/UpdateProductUsingRTK";

function App() {
  let dispatch = useDispatch();
  let infoData = useSelector((store) => {
    // console.log(store.info)
    return store.info;
  });

  let addressData = useSelector((store) => {
    return store.address;
  });

  let productData = useSelector((store) => {
    return store.product;
  });

  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <NavBar></NavBar>
              <Outlet></Outlet>
              {/* <Footer></Footer> */}
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            {/* <Route index element={<ShowAllProducts></ShowAllProducts>}></Route> */}
            <Route
              index
              element={<ShowAllProductsUsingRTK></ShowAllProductsUsingRTK>}
            ></Route>
            <Route
              path=":id"
              element={<ViewProductUsingRTK></ViewProductUsingRTK>}
            ></Route>
            {/* <Route path=":id" element={<ViewProducts></ViewProducts>}></Route> */}
            <Route
              path="create"
              element={<CreateProductUsingRTK></CreateProductUsingRTK>}
            ></Route>
            {/* <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route> */}
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route index element={<div>Update Products</div>}></Route>
              {/* <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route> */}
              <Route
                path=":id"
                element={<UpdateProductUsingRTK></UpdateProductUsingRTK>}
              ></Route>
            </Route>
          </Route>
        </Route>
      </Routes>
      {/* <div>{infoData.name}</div>
      <button
        onClick={() => {
          dispatch(changeName("dawei"));
        }}
      >
        Change Name
      </button>
      <div>{addressData.city}</div>
      <button
        onClick={() => {
          dispatch(changeCity("Rio"));
        }}
      >
        Change City
      </button>
      <div>{productData.name}</div>
      <button
        onClick={() => {
          dispatch(changeProductName("samsung"));
        }}
      >
        Change Name
      </button> */}
    </div>
  );
}

export default App;
