import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  city: "pinha",
  province: 3,
};

export const addressSlice = createSlice({
  name: "address",
  initialState: initialStateValue,
  reducers: {
    changeCity: (state, action) => {
      state.city = action.payload;
    },
  },
});

export const { changeCity } = addressSlice.actions;

export default addressSlice.reducer;
