import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  name: "chao",
  //   age:32,
  //   isMarried:false,
};

export const counterSlice = createSlice({
  name: "infoSlice",
  initialState: initialStateValue,
  reducers: {
    changeName: (state,action) => {
      state.name = action.payload
    },
  },
});

export const { changeName } = counterSlice.actions;

export default counterSlice.reducer;
