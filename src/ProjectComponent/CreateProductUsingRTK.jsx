import { Formik, Form } from "formik";
import React, { useEffect } from "react";
import * as yup from "yup";
import FormikInput from "../Formik/FormikInput";
import FormikCheckBox from "../Formik/FormikCheckBox";
import FormikSelect from "../Formik/FormikSelect";
import axios from "axios";
import htmlDateFormat from "../utils/htmlDateFormat";
import ProductForm from "./ProductForm";
import { useNavigate } from "react-router-dom";

import { useCreateProductsMutation } from "../services/api/productService";

const CreateProductUsingRTK = () => {
  let navigate = useNavigate();

  let [
    createProduct,
    {
      isSuccess: isSuccessCreateProduct,
      isError: isErrorCreateProduct,
      isLoading: isLoadingCreateProduct,
      error: errorCreateProduct,
    },
  ] = useCreateProductsMutation();

  useEffect(() => {
    if (isSuccessCreateProduct) {
      console.log("successfully created");
      navigate("/products");
    }
  }, [isSuccessCreateProduct]);
  useEffect(() => {
    if (isErrorCreateProduct) {
      console.log(errorCreateProduct?.error);
    }
  }, [isErrorCreateProduct, errorCreateProduct]);

  let onSubmit = async (values, other) => {
    let body = values;
    createProduct(body);
  };

  return (
    <div>
      <ProductForm
        buttonName="Create Product"
        onSubmit={onSubmit}
        isLoading={isLoadingCreateProduct}
      ></ProductForm>
    </div>
  );
};

export default CreateProductUsingRTK;
