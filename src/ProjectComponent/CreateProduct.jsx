import { Formik, Form } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "../Formik/FormikInput";
import FormikCheckBox from "../Formik/FormikCheckBox";
import FormikSelect from "../Formik/FormikSelect";
import axios from "axios";
import htmlDateFormat from "../utils/htmlDateFormat";
import ProductForm from "./ProductForm";
import { useNavigate } from "react-router-dom";
import { hitApi } from "../services/hitapi";

const CreateProduct = () => {
  let navigate = useNavigate();

  let onSubmit = async (values, other) => {
    try {
      let output = await hitApi({
        method: "POST",
        url: "/products",
        data: values,
      });
      navigate(`/products`);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <ProductForm
        buttonName="Create Product"
        onSubmit={onSubmit}
      ></ProductForm>
    </div>
  );
};

export default CreateProduct;
