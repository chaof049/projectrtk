import { Formik, Form } from "formik";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import FormikInput from "../Formik/FormikInput";
import FormikCheckBox from "../Formik/FormikCheckBox";
import FormikSelect from "../Formik/FormikSelect";

import { useNavigate, useParams } from "react-router-dom";
import htmlDateFormat from "../utils/htmlDateFormat";
import ProductForm from "./ProductForm";
import { hitApi } from "../services/hitapi";

const UpdateProduct = () => {
  let params = useParams();
  let navigate = useNavigate();
  let [product, setProduct] = useState({});

  let getProduct = async () => {
    try {
      let output = await hitApi({
        method: "GET",
        url: `/products/${params.id}`,
      });
      setProduct(output.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getProduct();
  }, []);

  let onSubmit = async (values, other) => {
    try {
      let output = await hitApi({
        method: "PATCH",
        url: `/products/${params.id}`,
        data: values,
      });
      navigate(`/products/${params.id}`);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <ProductForm
        buttonName="Update"
        onSubmit={onSubmit}
        product={product}
      ></ProductForm>
    </div>
  );
};

export default UpdateProduct;
