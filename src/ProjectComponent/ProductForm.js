import { Formik, Form } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "../Formik/FormikInput";
import FormikCheckBox from "../Formik/FormikCheckBox";
import FormikSelect from "../Formik/FormikSelect";
import htmlDateFormat from "../utils/htmlDateFormat";
import { companyOptions } from "../config/config";

const ProductForm = ({
  buttonName = "Create Product",
  onSubmit = () => {},
  product = {},
  isLoading = false,
}) => {
  let initialValues = {
    name: product.name || "",
    quantity: product.quantity || 0,
    price: product.price || 0,
    featured: product.featured || false,
    productImage: product.productImage || "",
    manufactureDate: htmlDateFormat(product.manufactureDate || new Date()),
    company: product.company || "apple",
  };

  let validationSchema = yup.object({
    name: yup.string(),
    quantity: yup.number(),
    price: yup.number(),
    featured: yup.boolean(),
    productImage: yup.string(),
    manufactureDate: yup.string(),
    company: yup.string(),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name"
                type="text"
                required={true}
              ></FormikInput>
              <FormikInput
                name="quantity"
                label="Quantity"
                type="number"
              ></FormikInput>
              <FormikInput
                name="price"
                label="Price"
                type="number"
              ></FormikInput>
              <FormikCheckBox name="featured" label="Featured"></FormikCheckBox>
              <FormikInput
                name="productImage"
                label="Product Image"
                type="text"
              ></FormikInput>
              <FormikInput
                name="manufactureDate"
                label="Manufacture Date"
                type="date"
              ></FormikInput>
              <FormikSelect
                name="company"
                label="Company"
                options={companyOptions}
              ></FormikSelect>
              <button type="submit">
                {isLoading ? (
                  <div>{buttonName}...</div>
                ) : (
                  <div>{buttonName}</div>
                )}
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default ProductForm;
