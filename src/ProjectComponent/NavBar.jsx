import React from "react";
import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <div>
      <nav className="bg-gray-600 text-red-50">
        <NavLink to="/products" className="ml-4">
          Products
        </NavLink>
        <NavLink to="/products/create" className="ml-4">
          Create Products
        </NavLink>
        {/* <NavLink to="localhost:3000/products" className={"ml-4"}>Update Products</NavLink>
        <NavLink to="localhost:3000/products" className={"ml-4"}>Detail</NavLink> */}
      </nav>
    </div>
  );
};

export default NavBar;
