import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useReadProductsByIdQuery } from "../services/api/productService";

const ViewProductUsingRTK = () => {
  let params = useParams();

  let {
    isError: isErrorReadDetail,
    isLoading: isLoadingReadDetail,
    data: dataReadDetail,
    error: errorReadDetail,
  } = useReadProductsByIdQuery(params.id);

  let product = dataReadDetail?.data || {};

  useEffect(() => {
    if (isErrorReadDetail) {
      console.log(errorReadDetail.error);
    }
  }, [isErrorReadDetail, errorReadDetail]);

  return (
    <div>
      {isLoadingReadDetail ? (
        <div>loading...</div>
      ) : (
        <div>
          <p>product name:{product?.name}</p>
          <p>product company:{product?.company}</p>
          <p>is featured:{product?.featured ? "yes" : "no"}</p>
          <p>
            manufactureDate:
            {new Date(product?.manufactureDate).toLocaleDateString()}
          </p>
        </div>
      )}
    </div>
  );
};

export default ViewProductUsingRTK;
