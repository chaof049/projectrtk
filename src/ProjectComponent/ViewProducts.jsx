import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { hitApi } from "../services/hitapi";

const ViewProducts = () => {
  let params = useParams();
  let [product, setProduct] = useState({});

  let getProduct = async () => {
    try {
      let output = await hitApi({
        method: "GET",
        url: `/products/${params.id}`,
      });
      setProduct(output.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div>
      <p>product name:{product.name}</p>
      <p>product company:{product.company}</p>
      <p>is featured:{product.featured ? "yes" : "no"}</p>
      <p>
        manufactureDate:{new Date(product.manufactureDate).toLocaleDateString()}
      </p>
    </div>
  );
};

export default ViewProducts;
