import React, { useEffect } from "react";

import { useNavigate, useParams } from "react-router-dom";
import ProductForm from "./ProductForm";
import {
  useReadProductsByIdQuery,
  useUpdateProductsMutation,
} from "../services/api/productService";

const UpdateProductUsingRTK = () => {
  let params = useParams();
  let navigate = useNavigate();

  let [
    updateProduct,
    {
      isSuccess: isSuccessUpdateProduct,
      isError: isErrorUpdateProduct,
      isLoading: isLoadingUpdateProduct,
      error: errorUpdateProduct,
    },
  ] = useUpdateProductsMutation();

  let {
    isError: isErrorReadByIdData,
    data: dataReadByIdData,
    error: errorReadByIdData,
  } = useReadProductsByIdQuery(params.id);

  let product = dataReadByIdData?.data || {};

  useEffect(() => {
    if (isSuccessUpdateProduct) {
      console.log("successfully created");
    }
  }, [isSuccessUpdateProduct]);
  useEffect(() => {
    if (isErrorUpdateProduct) {
      console.log(errorUpdateProduct?.error);
    }
  }, [isErrorUpdateProduct, errorUpdateProduct]);

  useEffect(() => {
    if (isErrorReadByIdData) {
      console.log(errorReadByIdData?.error);
    }
  }, [isErrorReadByIdData, errorReadByIdData]);

  let onSubmit = async (values, other) => {
    updateProduct({ id: params.id, body: values });
    navigate(`/products/${params.id}`);
  };

  return (
    <div>
      <ProductForm
        buttonName="Update"
        onSubmit={onSubmit}
        product={product}
        isLoading={isLoadingUpdateProduct}
      ></ProductForm>
    </div>
  );
};

export default UpdateProductUsingRTK;
