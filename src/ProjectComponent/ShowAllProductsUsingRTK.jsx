import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  useDeleteProductsMutation,
  useReadProductsQuery,
} from "../services/api/productService";

const ShowAllProductsUsingRTK = () => {
  let [deleteId, setDeleteId] = useState(" ");
  let {
    isError: isErrorShowProduct,
    isLoading: isLoadingShowProduct,
    data: dataShowProduct,
    error: errorShowProduct,
  } = useReadProductsQuery();

  let [
    deleteProduct,
    {
      isError: isErrorDeleteProduct,
      isSuccess: isSuccessDeleteProduct,
      isLoading: isLoadingDeleteProduct,
      error: errorDeleteProduct,
    },
  ] = useDeleteProductsMutation();

  //   console.log(data?.data?.results);

  useEffect(() => {
    if (isErrorShowProduct) {
      console.log(errorShowProduct?.error);
    }
  }, [isErrorShowProduct, errorShowProduct]);

  useEffect(() => {
    if (isErrorDeleteProduct) {
      console.log(errorDeleteProduct?.error);
    }
  }, [isErrorDeleteProduct, errorDeleteProduct]);

  useEffect(() => {
    if (isSuccessDeleteProduct) {
      console.log("product is deleted successfully");
    }
  }, [isSuccessDeleteProduct]);

  let products = dataShowProduct?.data?.results || [];

  let navigate = useNavigate();

  let handleView = (item) => {
    return () => {
      navigate(`/products/${item._id}`);
    };
  };
  let handleEdit = (item) => {
    return () => {
      navigate(`/products/update/${item._id}`);
    };
  };
  return (
    <div>
      {isLoadingShowProduct ? (
        <div>
          <h1>...Loading</h1>
        </div>
      ) : (
        <div>
          {products?.map((item, i) => {
            return (
              <div className="border bg-gray-300 mb-3">
                <img
                  src={item.productImage}
                  alt="product image"
                  className="w-10 h-10"
                ></img>
                <br></br>
                name:{item.name}
                <br></br>
                price:{item.price}
                <br></br>
                quantity:{item.quantity}
                <br></br>
                company:{item.company}
                <br></br>
                <button
                  onClick={async (e) => {
                    deleteProduct(item._id);
                    setDeleteId(item._id);
                  }}
                  className="bg-red-500 mr-3 p-1"
                >
                  {isLoadingDeleteProduct && item._id === deleteId
                    ? "Deleting..."
                    : "Delete"}
                </button>
                <button
                  onClick={handleView(item)}
                  className="bg-indigo-500 mr-3 p-1"
                >
                  View
                </button>
                <button
                  onClick={handleEdit(item)}
                  className="bg-green-500 mr-3 p-1"
                >
                  Edit
                </button>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default ShowAllProductsUsingRTK;
