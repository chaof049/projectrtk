import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { hitApi } from "../services/hitapi";

const ShowAllProducts = () => {
  let [products, setProducts] = useState([]);

  let navigate = useNavigate();

  let getProducts = async () => {
    try {
      let output = await hitApi({
        method: "GET",
        url: "/products",
      });
      setProducts(output.data.data.results);
    } catch (error) {
      console.log(error.message);
    }
  };
  //   let handleDelete = async (id) => {
  //     try {
  //       let output = await axios({
  //         method: "DELETE",
  //         url: `http://localhost:8000/api/v1/products/${id}`,
  //       });
  //       getProducts()
  //     } catch (error) {
  //       console.log(error.message);
  //     }
  //   };
  useEffect(() => {
    getProducts();
  }, []);

  let handleView = (item) => {
    return () => {
      navigate(`/products/${item._id}`);
    };
  };
  let handleEdit = (item) => {
    return () => {
      navigate(`/products/update/${item._id}`);
    };
  };
  return (
    <div>
      {products.map((item, i) => {
        return (
          <div className="border bg-gray-300 mb-3">
            <img
              src={item.productImage}
              alt="product image"
              className="w-10 h-10"
            ></img>
            <br></br>
            name:{item.name}
            <br></br>
            price:{item.price}
            <br></br>
            quantity:{item.quantity}
            <br></br>
            company:{item.company}
            <br></br>
            <button
              onClick={async () => {
                let output = await hitApi({
                  method: "DELETE",
                  url: `/products/${item._id}`,
                });
                getProducts();
              }}
              className="bg-red-500 mr-3 p-1"
            >
              Delete
            </button>
            <button
              onClick={handleView(item)}
              className="bg-indigo-500 mr-3 p-1"
            >
              View
            </button>
            <button
              onClick={handleEdit(item)}
              className="bg-green-500 mr-3 p-1"
            >
              Edit
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ShowAllProducts;
