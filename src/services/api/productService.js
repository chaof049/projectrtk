import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "../../config/config";

export const productApi = createApi({
  reducerPath: "productApi",
  baseQuery: fetchBaseQuery({
    baseUrl: baseUrl,
  }),
  tagTypes: ["readProducts", "readProductsById"],

  endpoints: (builder) => ({
    readProducts: builder.query({
      query: () => {
        return {
          url: "/products",
          method: "GET",
        };
      },
      providesTags: ["readProducts"],
    }),
    readProductsById: builder.query({
      query: (id) => {
        return {
          url: `/products/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readProductsById"],
    }),
    deleteProducts: builder.mutation({
      query: (id) => {
        return {
          url: `/products/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readProducts"],
    }),
    createProducts: builder.mutation({
      query: (body) => {
        return {
          url: "/products",
          method: "POST",
          body: body,
        };
      },
      invalidatesTags: ["readProducts"],
    }),
    updateProducts: builder.mutation({
      query: (info) => {
        return {
          url: `/products/${info.id}`,
          method: "PATCH",
          body: info.body,
        };
      },
      invalidatesTags: ["readProducts", "readProductsById"],
    }),
  }),
});

export const {
  useReadProductsQuery,
  useDeleteProductsMutation,
  useReadProductsByIdQuery,
  useCreateProductsMutation,
  useUpdateProductsMutation,
} = productApi;
